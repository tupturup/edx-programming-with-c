﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "Mate";
            string lastName = "Buba";
            DateTime birthdate = new DateTime(1990, 5, 12);
            string addressLine1 = "Genergatan";
            string addressLine2 = "2";
            string city = "Paris";
            string state = "Noidea";
            int zip = 34820;
            string country = "France";

            Console.WriteLine(" Name and surname: {0} {1}\n Birthdate: {2}\n Address: {3} {4}\n City: {5}\n State: {6}\n Zip: {7}\n Country: {8}", firstName, lastName, birthdate, addressLine1, addressLine2, city, state, zip, country);
            Console.ReadLine();
        }
    }
}