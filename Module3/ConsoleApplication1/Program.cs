﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BirthdayValidated();
            }
            catch (Exception)
            {
                Console.WriteLine("BirthdayValidated exception thrown!");
            }

            GetStudentInformation();
            GetTeacherInformation();
            GetUProgramInfo();
            GetDegreeInfo();
            GetCourseInfo();

        }

        static void GetStudentInformation()
        {
            int address2 = 0;
            int zip = 0;
            int count = 3;

            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the student's last name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the birthdate: ");
            string birthdate = Console.ReadLine();
            Console.WriteLine("Enter the street address: ");
            string address1 = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Enter the address number: ");
                    address2 = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected!");
                    count++;
                }
                count = 3;
            }

            Console.WriteLine("Enter the city: ");
            string city = Console.ReadLine();
            Console.WriteLine("Enter the state: ");
            string state = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Enter the zip/postal: ");
                    zip = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected!");
                    count++;
                }
                count = 3;
            }

            Console.WriteLine("Enter the country: ");
            string country = Console.ReadLine();
            Console.WriteLine();

            PrintStudentDetails(firstName, lastName, birthdate, address1, address2, city, state, zip, country);
        }

        static void PrintStudentDetails(string first, string last, string birthday, string address1, int address2, string city, string state, int zip, string country)
        {
            Console.WriteLine("Name: {0} {1}", first, last);
            Console.WriteLine("Birthdate: {0}", birthday);
            Console.WriteLine("Address: {0} {1}, {2} {3}, in {4} {5}", address1, address2, zip, city, state, country);
            Console.WriteLine();
        }

        static void GetTeacherInformation()
        {
            int address2 = 0;
            int zip = 0;
            int count = 3;

            Console.WriteLine("Enter the teacher's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's last name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the birthdate: ");
            string birthdate = Console.ReadLine();
            Console.WriteLine("Enter the street address: ");
            string address1 = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Enter the address number: ");
                    address2 = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected!");
                    count++;
                }
                count = 3;
            }

            Console.WriteLine("Enter the city: ");
            string city = Console.ReadLine();
            Console.WriteLine("Enter the state: ");
            string state = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Enter the zip/postal: ");
                    zip = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected!");
                    count++;
                }
                count = 3;
            }

            Console.WriteLine("Enter the country: ");
            string country = Console.ReadLine();
            Console.WriteLine();

            PrintTeacherDetails(firstName, lastName, birthdate, address1, address2, city, state, zip, country);
        }

        static void PrintTeacherDetails(string first, string last, string birthday, string address1, int address2, string city, string state, int zip, string country)
        {
            Console.WriteLine("Name: {0} {1}", first, last);
            Console.WriteLine("Birthdate: {0}", birthday);
            Console.WriteLine("Address: {0} {1}, {2} {3}, in {4} {5}", address1, address2, zip, city, state, country);
            Console.WriteLine();
        }

        static void GetUProgramInfo()
        {
            Console.WriteLine("Program name: ");
            string pname = Console.ReadLine();
            Console.WriteLine("Department head: ");
            string head = Console.ReadLine();
            Console.WriteLine("Degrees: ");
            string degrees = Console.ReadLine();
            Console.WriteLine();

            PrintUProgramInfo(pname, head, degrees);
        }

        static void PrintUProgramInfo(string name, string head, string degrees)
        {
            Console.WriteLine("Program name: {0}", name);
            Console.WriteLine("Department head: {0}", head);
            Console.WriteLine("Degrees: {0}", degrees);
            Console.WriteLine();
        }

        static void GetDegreeInfo()
        {
            int credits = 0;
            int count = 3;

            Console.WriteLine("Degree name: ");
            string degName = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Credits required: ");
                    credits = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected!");
                    count++;
                }
            }
            Console.WriteLine();

            PrintDegreeInfo(degName, credits);
        }

        static void PrintDegreeInfo(string degreeName, int credits)
        {
            Console.WriteLine("Degree name: {0}", degreeName);
            Console.WriteLine("Credits required: {0}", credits);
            Console.WriteLine();
        }

        static void GetCourseInfo()
        {
            int credits = 0;
            int weeks = 0;
            int count = 3;

            Console.WriteLine("Course name: ");
            string cname = Console.ReadLine();

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Credits: ");
                    credits = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected");
                    count++;
                }
                count = 3;
            }

            for (int i = 0; i < count; i++)
            {
                try
                {
                    Console.WriteLine("Duration in weeks: ");
                    weeks = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Number expected");
                    count++;
                }
            }

            Console.WriteLine("Teacher: ");
            string teacher = Console.ReadLine();
            Console.WriteLine();

            PrintCourseInfo(cname, credits, weeks, teacher);
        }

        static void PrintCourseInfo(string name, int credits, int duration, string teacher)
        {
            Console.WriteLine("Course name: {0}", name);
            Console.WriteLine("Credits: {0}", credits);
            Console.WriteLine("Duration: {0}", duration);
            Console.WriteLine("Teacher: {0}", teacher);
            Console.WriteLine();
        }

        static void BirthdayValidated()
        {
            throw new NotImplementedException();
        }
    }
}