﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    struct Student
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string Country { get; set; }

        public Student(string firstName, string secondName, DateTime birthdate, string address1, string address2, string city, string state, int zip, string country)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Birthdate = birthdate;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
        }
    }

    struct Teacher
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime Birthdate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
        public string Country { get; set; }

        public Teacher(string firstName, string secondName, DateTime birthdate, string address1, string address2, string city, string state, int zip, string country)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Birthdate = birthdate;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
        }
    }

    struct UProgramInfo
    {
        public string Name { get; set; }
        public string Head { get; set; }
        public string Degrees { get; set; }

        public UProgramInfo(string name, string head, string degrees)
        {
            this.Name = name;
            this.Head = head;
            this.Degrees = degrees;
        }
    }

    struct DegreeInfo
    {
        public string Name { get; set; }
        public int Credits { get; set; }

        public DegreeInfo(string name, int credits)
        {
            this.Name = name;
            this.Credits = credits;
        }
    }

    struct CourseInfo
    {
        public string Name { get; set; }
        public int Credits { get; set; }
        public int Duration { get; set; }
        public string Teacher { get; set; }

        public CourseInfo(string name, int credits, int duration, string teacher)
        {
            this.Name = name;
            this.Credits = credits;
            this.Duration = duration;
            this.Teacher = teacher;
        }
    }

    class Program
    {
        static void Main()
        {
            Student[] stArray = new Student[5];

            stArray[0].FirstName = "Mark";
            stArray[0].SecondName = "Smith";
            stArray[0].Birthdate = new DateTime(1990, 04, 12);
            stArray[0].City = "Paris";

            Console.WriteLine(stArray[0].FirstName);
            Console.WriteLine(stArray[0].SecondName);
            Console.WriteLine(stArray[0].Birthdate);
            Console.WriteLine(stArray[0].City);
            Console.ReadLine();
        }
    }
}