﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class CourseInfo
    {
        private string _name;
        private int _credits;
        private int _duration;
        public Student[] students = new Student[3];
        public Teacher[] teachers = new Teacher[3];

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public int Credits
        {
            get
            {
                return _credits;
            }

            set
            {
                _credits = value;
            }
        }

        public int Duration
        {
            get
            {
                return _duration;
            }

            set
            {
                _duration = value;
            }
        }

        public CourseInfo(string name, int credits, int duration)
        {
            this.Name = name;
            this.Credits = credits;
            this.Duration = duration;
        }
    }
}
