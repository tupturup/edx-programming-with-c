﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Teacher
    {
        private string _firstName;
        private string _secondName;
        private DateTime _birthdate;
        private string _address1;
        private string _address2;
        private string _city;
        private string _state;
        private int _zip;
        private string _country;

        public string FirstName
        {
            get
            {
                return _firstName;
            }

            set
            {
                _firstName = value;
            }
        }

        public string SecondName
        {
            get
            {
                return _secondName;
            }

            set
            {
                _secondName = value;
            }
        }

        public DateTime Birthdate
        {
            get
            {
                return _birthdate;
            }

            set
            {
                _birthdate = value;
            }
        }

        public string Address1
        {
            get
            {
                return _address1;
            }

            set
            {
                _address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _address2;
            }

            set
            {
                _address2 = value;
            }
        }

        public string City
        {
            get
            {
                return _city;
            }

            set
            {
                _city = value;
            }
        }

        public string State
        {
            get
            {
                return _state;
            }

            set
            {
                _state = value;
            }
        }

        public int Zip
        {
            get
            {
                return _zip;
            }

            set
            {
                _zip = value;
            }
        }

        public string Country
        {
            get
            {
                return _country;
            }

            set
            {
                _country = value;
            }
        }

        public Teacher(string firstName, string secondName, DateTime birthdate, string address1, string address2, string city, string state, int zip, string country)
        {
            this.FirstName = firstName;
            this.SecondName = secondName;
            this.Birthdate = birthdate;
            this.Address1 = address1;
            this.Address2 = address2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Country = country;
        }
    }
}
