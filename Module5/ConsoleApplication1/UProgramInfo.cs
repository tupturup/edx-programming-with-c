﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class UProgramInfo
    {
        private string _name;
        private string _head;
        public DegreeInfo degree;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Head
        {
            get
            {
                return _head;
            }

            set
            {
                _head = value;
            }
        }


        public UProgramInfo(string name, string head)
        {
            this.Name = name;
            this.Head = head;
        }
    }
}
