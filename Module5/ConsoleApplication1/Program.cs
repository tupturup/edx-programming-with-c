﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ConsoleApplication1
{

    class Program
    {
        static void Main()
        {
            Student student1 = new Student("Bla", "Blabla", new DateTime(1990, 7, 12), "Street 1", "apt 2", "Some city", "And state", 23131, "aand country");
            Student student2 = new Student("Das", "Sasd", new DateTime(1990, 5, 21), "Dafasf", "Oasdas", "Dsa", "Hsg", 4234, "Orsf");
            Student student3 = new Student("Hfg", "Eter", new DateTime(1990, 8, 12), "Rerg", "Ggdfg", "Ser", "Dfg", 4234, "Sekk");

            Teacher teacher1 = new Teacher("Ijf", "Didf", new DateTime(1970, 1, 29), "Sdfo", "Aie", "Psf", "Kkf", 4234, "Uls");

            CourseInfo courseCsharp = new CourseInfo("Programming with C#", 7, 2);
            courseCsharp.students[0] = student1;
            courseCsharp.students[1] = student2;
            courseCsharp.students[2] = student3;
            courseCsharp.teachers[0] = teacher1;

            DegreeInfo degree1 = new DegreeInfo("Bachelor", 180);
            degree1.course = courseCsharp;

            UProgramInfo prog1 = new UProgramInfo("Information technology", "head who");
            prog1.degree = degree1;

            Console.WriteLine("The {0} program contains the {1} of Science degree", prog1.Name, prog1.degree.Name);
            Console.WriteLine("The {0} of science degree  contains the course  {1}", prog1.degree.Name, prog1.degree.course.Name);
            Console.WriteLine("The {0} course contains {1} students", prog1.degree.course.Name, Student.Count());
            Console.Read();
        }
    }
}