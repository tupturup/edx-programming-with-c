﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{

    public class DegreeInfo
    {
        private string _name;
        private int _credits;
        public CourseInfo course;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public int Credits
        {
            get
            {
                return _credits;
            }

            set
            {
                _credits = value;
            }
        }

        public CourseInfo Course
        {
            get
            {
                return course;
            }

            set
            {
                course = value;
            }
        }

        public DegreeInfo(string name, int credits)
        {
            this.Name = name;
            this.Credits = credits;
            this.Course = course;
        }
    }
    }
